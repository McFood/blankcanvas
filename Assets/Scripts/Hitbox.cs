﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hitbox : MonoBehaviour
{
    public Interactable interactable {get; private set;}

    void Awake()
    {
        interactable = gameObject.GetComponentInParent<Interactable>();
    }


    // public void Interact()
    // {
    //     if (interactable != null)
    //     {
    //         interactable.Interact();
    //     }
    // }

    // public void Click()
    // {
    //     if (interactable != null)
    //     {
    //         interactable.Click();
    //     }
    // }
}
