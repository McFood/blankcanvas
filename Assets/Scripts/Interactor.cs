﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    public float MaxInteractDistance = 2.0f;
    public float InteractSphereRadius = 0.6f;

    private RaycastHit[] possibleInteractables = new RaycastHit[20];

    Interactable hoveringInteractable;

    void DoHighlight(Interactable interactable){
        // same item, no change
        if(hoveringInteractable == interactable) return;

        // if(interactable == null) return;

        if(hoveringInteractable != null){
            hoveringInteractable.Highlight(false);
        }

        hoveringInteractable = interactable;
        
        if(hoveringInteractable != null){
            hoveringInteractable.Highlight(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        bool interacting = Input.GetKeyUp(KeyCode.E);
        bool clicking = Input.GetMouseButtonUp(0);

        Vector3 castPos = transform.position;
        Vector3 castCenter = castPos + transform.forward;
        Ray ray = new Ray(castPos, transform.forward);

        // Check all objects in a sphere cast for more forgiving interacting.
        int hits = Physics.SphereCastNonAlloc(
            ray, InteractSphereRadius, possibleInteractables, MaxInteractDistance);

        Interactable closestInteractable = null;
        float closestInteractAngle = 90.0f;

        for (int i = 0; i < hits; i++)
        {
            RaycastHit hit = possibleInteractables[i];
            GameObject hitObject = hit.collider.gameObject;

            // check the current gameobject, of if that is null, check its parent.
            Interactable interactable = hitObject.GetComponent<Interactable>()
                ?? hitObject.GetComponentInParent<Interactable>();

            // Not interactable, check the other raycast hits.
            if (interactable == null) continue;

            // Angle between center of camera to center of hitObject.
            float hitAngle = Vector3.Angle(
                castCenter - castPos,
                hitObject.transform.position - castPos);
            // If this interactable is closer than the last, save it instead.
            if (hitAngle < closestInteractAngle)
            {
                closestInteractable = interactable;
                closestInteractAngle = hitAngle;
            }
        }

        if (closestInteractable != null)
        {
            if (interacting)
                closestInteractable.Interact();
            if (clicking)
                closestInteractable.Click();
        }
        // allow to be null
        DoHighlight(closestInteractable);
    }
}
