﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disappearer : Interactable
{
    public override void Interact()
    {
        base.Interact();
        gameObject.SetActive(false);
    }
}
