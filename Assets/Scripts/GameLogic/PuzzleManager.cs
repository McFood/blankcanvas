using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Linq;

[Serializable]
public class PuzzleManagerEvents{
    public UnityEvent OnAllSolved;
}

[Serializable]
public class PuzzleTag{
    public String globalTag;
    public HashSet<PuzzleElement> elements = new HashSet<PuzzleElement>();
    public UnityEvent OnSolved = new UnityEvent();
    public bool isSolved = false;

    public void Solve()
    {
        isSolved = true;
        OnSolved.Invoke();
    }

}


// puzzle elements register themselves with the puzzle manager
// once all registered are solved it will invoke the solved event
public class PuzzleManager : MonoBehaviour
{

    public PuzzleManagerEvents puzzleManagerEvents;

    public HashSet<PuzzleElement> puzzleElements = new HashSet<PuzzleElement>();

    public List<PuzzleTag> globalTagEvents;

    // Singleton pattern
    private static PuzzleManager _instance;
    public static PuzzleManager instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<PuzzleManager>();
            }
            return _instance;
        }
    }

    public PuzzleTag GetPuzzleTag(string tagName)
    {
        if (string.IsNullOrEmpty(tagName))
        {
            Debug.LogError("Empty PuzzleTag Requested!");
        }
        foreach (PuzzleTag tag in globalTagEvents)
        {
            if (tag.globalTag.Equals(tagName))
            {
                return tag;
            }
        }
        PuzzleTag newTag = new PuzzleTag { globalTag = tagName };
        globalTagEvents.Add(newTag);
        return newTag;
    }

    public void RegisterPuzzleElement(PuzzleElement element){
        puzzleElements.Add(element);

        PuzzleTag tag = GetPuzzleTag(element.gloabalTag);
        tag.elements.Add(element);

        Debug.Log("resgistering: " + element.name, this);
        
    }

    public void CheckPuzzles(PuzzleElement element){

        PuzzleTag tag = GetPuzzleTag(element.gloabalTag);
        if (tag.elements.All(el => el.isSolved))
        {
            tag.Solve();
        }

        string log = "Elements: ";

        foreach(PuzzleElement test in puzzleElements){
            log += "[" + test.name + " - " + test.isSolved + "]";
        }

        Debug.Log(log);

        if(AllSolved()){
            Debug.Log("All puzzles solved!");
            puzzleManagerEvents.OnAllSolved.Invoke();
        }
    }

    // returns true if all puzzles are solved
    // otherwise returns false
    public bool AllSolved(){
        foreach(PuzzleElement element in puzzleElements){
            if(element.isSolved == false){
                return false;
            }
        }
        return true;
    }





}