using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;



[Serializable]
public class AudioTourEntry{

    public int number;
    public AudioClip clip;
}


// This is a base class for useable (paint brush)
// and missing pieces
public class AudioTourManager : MonoBehaviour
{

    public AudioClip errorSound;
    public AudioClip inputSound;

    public List<AudioTourEntry> entries;


    private static AudioTourManager _instance;
    public static AudioTourManager instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<AudioTourManager>();
            }
            return _instance;
        }
    }


}