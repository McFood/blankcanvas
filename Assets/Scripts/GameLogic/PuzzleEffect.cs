﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class PuzzleEffect : MonoBehaviour
{
    public string puzzleTag;
    public UnityEvent OnTagSolved;

    private void Awake()
    {
        PuzzleTag tag = PuzzleManager.instance.GetPuzzleTag(puzzleTag);
        tag.OnSolved.AddListener(() => OnTagSolved.Invoke());
    }
}
