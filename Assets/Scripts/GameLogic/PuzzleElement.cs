using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Puzzle elements are found by other classes in a game object
// if one is not found the game object is not part of a puzzle and 
// does not contribute to the overall puzzles needed to win
public class PuzzleElement : MonoBehaviour
{

    public bool isSolved {get; protected set;}

    public string gloabalTag;

    protected virtual void Start()
    {
        Debug.Log("Register");
        

        StartCoroutine(DelayRegister());
    }

    IEnumerator DelayRegister(){

        yield return new WaitForSeconds(2);

        PuzzleManager.instance.RegisterPuzzleElement(this);

    }

    public void Solve(bool solved){
        PuzzleManager.instance.RegisterPuzzleElement(this);
        isSolved = solved;

        if(isSolved){
            PuzzleManager.instance.CheckPuzzles(this);
        }

    }

}
