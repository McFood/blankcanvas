using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;



public class InventoryManager : MonoBehaviour
{

    // list of items

    // only show active one ( keep track of index)

    // move index with scroll

    List<InventoryItem> inventoryItems;
    int currentIndex = 0;

    public Transform holdPoint;
    public Transform rotator;
    public Transform fixedHoldPoint;
    public float inventroyRotatorAngle = 30;
    public float inventoryTime = 0.2f;
    public InventoryItem CurrentItem => inventoryItems[currentIndex];

    bool canScroll = true;
    float nextScrollTime;

    public float minScrollTime = 0.1f;

    public AnimationCurve grabCurve = AnimationCurve.EaseInOut(0,0,1,1);
    public AnimationCurve holsterCurve = AnimationCurve.EaseInOut(0,0,1,1);
    public AnimationCurve unholsterCurve = AnimationCurve.EaseInOut(0,0,1,1);

    // Singleton pattern
    private static InventoryManager _instance;
    public static InventoryManager instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<InventoryManager>();
            }
            return _instance;
        }
    }
    Quaternion inventoryRotation;
    void Awake(){

        inventoryRotation = Quaternion.Euler(inventroyRotatorAngle, 0 , 0);

        inventoryItems = new List<InventoryItem>();

        InventoryItem[] existingItems = holdPoint.GetComponentsInChildren<InventoryItem>();

        inventoryItems.AddRange(existingItems);
    }

    void Update(){
        // DoScrollTiming();
        CheckForScroll();
    }

    bool isLerping;
    float lerp;



    void SetNextScrollTime(){
        canScroll = false;
        nextScrollTime = Time.time + minScrollTime;
    }

    void CheckForScroll(){

        if(!canScroll) return;

        if (Input.GetAxis("Mouse ScrollWheel") > 0f ){
            ScrollNext();
            SetNextScrollTime();
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f ){
            ScrollPrevious();
            SetNextScrollTime();
        }
    }

    public void GhostHand(DropZone zone){
        InventoryItem item = ItemFromIndex(currentIndex);
        item.Ghost(zone);
    }


    // lerp out existing
    // lerp in new

    public InventoryItem ItemFromIndex(int index){
        if(index >= inventoryItems.Count) return null;

        return inventoryItems[index];
    }

    public void AddItem(InventoryItem addedItem){

        if(pickingUp != null) return;

        pickingUp = addedItem;

        addedItem.SetCollider(false);

        InventoryItem lastItem = ItemFromIndex(currentIndex);

        

        inventoryItems.Add(pickingUp);
        currentIndex = inventoryItems.Count - 1;

        if(lastItem != null) StartCoroutine(RotateDown(lastItem, null));

        StartCoroutine(PickUp());
        
    }

    public void PlaceInDropZone(DropZone zone){
        InventoryItem item = RemoveItem(currentIndex);

        if(item == null) {
            Debug.LogWarning("Cannot remove this item", this);
            return;
        };

        item.SetCollider(true);
        currentIndex = NextIndex();
        InventoryItem nextItem = ItemFromIndex(currentIndex);
        StartCoroutine(RotateUp(nextItem));

        zone.PlaceItem(item);      
    }

    InventoryItem RemoveItem(int index){
        InventoryItem item = inventoryItems[index];

        // can't drop this
        if(!item.isDropable) return null;

        inventoryItems.RemoveAt(index);
        item.gameObject.layer &= ~LayerMask.NameToLayer("Ignore Raycast");

        return item;

        // swap in blank?
    }

    int NextIndex(){
        if(inventoryItems.Count == 0) return 0;

        return Math.Abs((currentIndex + 1) % inventoryItems.Count);
    }

    InventoryItem pickingUp;

    float pickupLerp;
    IEnumerator PickUp(){

        Vector3 startPos = pickingUp.transform.position;
        Quaternion startRot = pickingUp.transform.rotation;

        

        pickupLerp = 0;
        while(pickupLerp < 1){
            pickupLerp += Time.deltaTime / 0.2f;

            float curveLerp = grabCurve.Evaluate(pickupLerp);

            Vector3 finalPos = fixedHoldPoint.position + fixedHoldPoint.rotation * Vector3.Scale(pickingUp.itemHoldPoint.localPosition, pickingUp.transform.localScale);
            Quaternion finalRot = fixedHoldPoint.rotation * Quaternion.Inverse(pickingUp.itemHoldPoint.localRotation);

            pickingUp.transform.position = Vector3.Lerp(startPos, finalPos, curveLerp);
            pickingUp.transform.rotation = Quaternion.Slerp(startRot, finalRot, curveLerp);
            pickingUp.transform.localScale = Vector3.Lerp(pickingUp.initialScale, Vector3.Scale(pickingUp.initialScale, pickingUp.itemHoldPoint.localScale), curveLerp); 
            yield return 0;
        }

        pickupLerp = 1;

        pickingUp.AttachToTransform(fixedHoldPoint);

        pickingUp = null;
        
        String log = "[" + currentIndex + "] Inventory items: ";

        for(int i = 0; i < inventoryItems.Count; i++){
            log += "[" + i + " - " + inventoryItems[i].name + "]";
        }

        Debug.Log(log);

    }

    float rotateDownLerp;
    IEnumerator RotateDown(InventoryItem downItem, InventoryItem upItem){


        

        // Debug.Log("Start rotate down");

        canScroll = false;

        rotateDownLerp = 0;

        SetItemScroll(downItem);

        if(downItem == null || downItem.skipInventoryAnimations) rotateDownLerp = 1;

        
        
        while(rotateDownLerp < 1){
            rotateDownLerp += Time.deltaTime / 0.2f;
            float curveLerp = holsterCurve.Evaluate(rotateDownLerp);
            rotator.localRotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(inventroyRotatorAngle,0,0), curveLerp);
            yield return 0;
        }

        HideItem(downItem);

        // Debug.Log("Finish rotate down");

        rotateDownLerp = 1;

        if(upItem == null){
            rotator.localRotation = Quaternion.identity;
            canScroll = true;
        }else{
            StartCoroutine(RotateUp(upItem));
        }
        
    }

    float rotateUpLerp;
    IEnumerator RotateUp(InventoryItem item){

        // Debug.Log("Start rotate up");

        rotateUpLerp = 0;

        SetItemScroll(item);
        ShowItem(item);

        if(item.skipInventoryAnimations) rotateUpLerp = 1;

        

        while(rotateUpLerp < 1){
            rotateUpLerp += Time.deltaTime / 0.2f;
            float curveLerp = unholsterCurve.Evaluate(rotateUpLerp);
            rotator.localRotation = Quaternion.Lerp(Quaternion.Euler(inventroyRotatorAngle,0,0), Quaternion.identity, curveLerp);
            yield return 0;
        }

        // Debug.Log("Finish rotate up");
        
        rotateUpLerp = 1;

        canScroll = true;

        SetItemFixed(item);
    }


    void ScrollNext(){
        // No scrolling if there is one or no items
        if(inventoryItems.Count == 0 || inventoryItems.Count == 1){
            return;
        }

        InventoryItem lastItem = ItemFromIndex(currentIndex);
        currentIndex = NextIndex();
        InventoryItem nextItem = ItemFromIndex(currentIndex);

        StartCoroutine(RotateDown(lastItem, nextItem));
    }

    void ScrollPrevious(){
        // No scrolling if there is one or no items
        if(inventoryItems.Count == 0 || inventoryItems.Count == 1){
            return;
        }

        InventoryItem lastItem = ItemFromIndex(currentIndex);
        currentIndex = (currentIndex - 1 + inventoryItems.Count) % inventoryItems.Count;
        InventoryItem nextItem = ItemFromIndex(currentIndex);

        StartCoroutine(RotateDown(lastItem, nextItem));
    }

    void ShowItem(InventoryItem item){

        if(item == null) return;
        item.Unholster();
        item.gameObject.SetActive(true);
    }

    void HideItem(InventoryItem item){
        if(item == null) return;
        item.Holster();
        item.gameObject.SetActive(false);
    }

    void SetItemFixed(InventoryItem item){
        if(item == null) return;

        item.AttachToTransform(fixedHoldPoint);

    }

    void SetItemScroll(InventoryItem item){

        if(item == null) return;


        Debug.Log("Scrolling " + item.name);

        item.AttachToTransform(holdPoint);
 
    }



}
