using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;



public class GameManager : MonoBehaviour
{

    public Material highlightMaterial;
    public Material ghostMaterial;

    // Singleton pattern
    private static GameManager _instance;
    public static GameManager instance
    {
        get
        {
            if ( _instance == null )
            {
                _instance = FindObjectOfType<GameManager>();
            }
            return _instance;
        }
    }



}