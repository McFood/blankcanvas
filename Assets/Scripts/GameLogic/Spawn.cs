﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawn : MonoBehaviour
{
    public AudioSource roomMusic;

    public UnityEvent OnSpawn;

    void Awake()
    {
        if (roomMusic == null)
        {
            roomMusic = GetComponent<AudioSource>();
            OnSpawn.AddListener(() =>
            {
                if (roomMusic != null)
                {
                    roomMusic.Play();
                }
            });
        }
    }

    public void SpawnUsed()
    {
        OnSpawn.Invoke();
    }
}
