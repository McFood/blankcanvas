using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;
using UnityStandardAssets.Characters.FirstPerson;



public class Player : MonoBehaviour
{


    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Player");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        WarpInteract.LoadAllScenes();

        DontDestroyOnLoad(this.gameObject);
    }

    public void GoToSpawn(string spawnName)
    {
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("Respawn");
        GameObject spawn = null;
        foreach (GameObject possibleSpawn in spawns)
        {
            if (possibleSpawn.name == spawnName)
            {
                spawn = possibleSpawn;
                break;
            }
        }
        if (spawn == null && spawns.Length > 0)
        {
            spawn = spawns[0];
            Debug.Log("Using default spawn: " + spawn.name);
        }
        if (spawn != null)
        {
            FirstPersonController fpsController = GetComponent<FirstPersonController>();
            CharacterController characterController = GetComponent<CharacterController>();
            characterController.enabled = false;
            transform.position = spawn.transform.position;
            transform.rotation = spawn.transform.rotation;
            characterController.enabled = true;
            fpsController.OverrideRotation();
            Spawn spawnScript = spawn.GetComponent<Spawn>();
            spawnScript.SpawnUsed();
        }
        else
        {
            Debug.Log("No spawn point found, player might fall");
        }
    }

}