﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WarpInteract : Interactable
{

    public string SceneName;
    public string SpawnName = "PlayerSpawn";

    public override void Interact()
    {
        base.Interact();

        GameObject playerGO = GameObject.FindGameObjectWithTag("Player");
        Player player = playerGO.GetComponent<Player>();

        Scene nextScene = SceneManager.GetSceneByName(SceneName);
        if (!nextScene.IsValid())
        {
            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
            sceneLoad.completed += (_) =>
            {
                nextScene = SceneManager.GetSceneByName(SceneName);
                SceneManager.SetActiveScene(nextScene);
                player.GoToSpawn(SpawnName);
            };
        } else
        {
            SceneManager.SetActiveScene(nextScene);
            player.GoToSpawn(SpawnName);
        }
    }


    public static void LoadAllScenes()
    {
        HashSet<string> sceneNames = new HashSet<string>();
        WarpInteract[] warps = GameObject.FindObjectsOfType<WarpInteract>();
        foreach (WarpInteract warp in warps)
        {
            sceneNames.Add(warp.SceneName);
        }
        LoadAllScenes(sceneNames.ToList());
    }

    public static void LoadAllScenes(List<string> sceneNames)
    {
        if (sceneNames.Count == 0)
        {
            return;
        }
        string nextSceneName = sceneNames[0];
        // Remove the scene we're checking from the list of scenes to load.
        sceneNames.RemoveAt(0);

        Scene nextScene = SceneManager.GetSceneByName(sceneNames[0]);
        if (!nextScene.IsValid()) {
            // Scene hasn't been loaded before, load it now.
            AsyncOperation sceneLoad = SceneManager.LoadSceneAsync(sceneNames[0], LoadSceneMode.Additive);

            // Once loading is complete, call this method again with the new list of scene names.
            sceneLoad.completed += (_) =>
            {
                LoadAllScenes(sceneNames);
            };
        } else {
            // Scene is already loaded, check the remaining scenes.
            LoadAllScenes(sceneNames);
        }

    }


}
