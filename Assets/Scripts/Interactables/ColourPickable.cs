﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourPickable : Interactable
{
    public Material color;

    protected override void Awake()
    {
        base.Awake();
        if (color == null)
        {
            color = GetComponent<Renderer>().material;
        }
    }

    public override void Interact()
    {
        base.Interact();
        InventoryItem currentItem = InventoryManager.instance.CurrentItem;
        if (currentItem == null)
            return;

        PaintBrush paintBrush = currentItem.GetComponent<PaintBrush>();
        if (paintBrush == null)
            return;

        paintBrush.Color = color;
    }
}
