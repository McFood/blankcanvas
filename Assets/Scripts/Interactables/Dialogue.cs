using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

using UnityEngine.Events;


// This is a base class for useable (paint brush)
// and missing pieces
[RequireComponent(typeof(AudioSource))]
public class Dialogue : Interactable
{

    AudioSource source;

    public AudioClip[] clips;
    int index;

    protected override void Awake(){

        source = GetComponent<AudioSource>();

        base.Awake();
    }

    public void PlayNextSoundNoInterrupt(){
        if(source.isPlaying) return;
        PlayNextSound();
    }

    // no highlights for characters
    protected override void SetupHighlight(){}
    public override void Highlight(bool highlight){}

    public void PlayNextSound(){
        PlaySoundIndex(index);
        index++;

        // if we're at the end stay on the last clip
        if(index >= clips.Length){
            index = clips.Length-1;
        }
    }

    public void PlaySoundIndex(int index){
        Debug.Log("PLaying sound at index " + index);

        if(index >= clips.Length) {
            Debug.LogError("index outside clip range", this);
            return;
        }

        if(clips[index] == null){
            Debug.LogError("clip is null for index " + index, this);
        }

        source.clip = clips[index];
        source.Play();

    }

    public override void Interact(){

        PlayNextSoundNoInterrupt();

        base.Interact();

    }


}