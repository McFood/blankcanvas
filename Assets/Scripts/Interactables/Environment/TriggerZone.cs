using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

using UnityEngine.Events;

[Serializable]
public class TriggerEvents{
    public UnityEvent onEnter;
    public UnityEvent onExit;
}


public class TriggerZone : MonoBehaviour
{

    public TriggerEvents triggerEvents;
    public bool onlyOnce;
    private bool doNotTrigger;

    Collider triggerZone;

    void Awake(){
        triggerZone = GetComponent<Collider>();

        if(triggerZone == null) Debug.LogError("No collider on trigger", this);
        if(!triggerZone.isTrigger) Debug.LogError("Trigger zone collider not trigger", this);
    }

    public void OnTriggerEnter(){

        triggerEvents.onEnter.Invoke();
    }

    public void OnTriggerExit(){

        if(doNotTrigger) return;

        triggerEvents.onExit.Invoke();

        if(onlyOnce) doNotTrigger = true;
    }


}
