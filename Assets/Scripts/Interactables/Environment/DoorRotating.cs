using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;


public class DoorRotating : BaseDoor
{



    protected Quaternion localClosed;
    protected Quaternion localOpen;

    protected override void Awake(){

        base.Awake();

        if(startOpen){
            localClosed = transform.localRotation * Quaternion.Euler(direction * distance);
            localOpen = transform.localRotation;
        }else{
            localClosed = transform.localRotation;
            localOpen = transform.localRotation * Quaternion.Euler(direction * distance);
        }
    }




    protected override void Movement(float lerp){
        transform.localRotation = Quaternion.SlerpUnclamped(localClosed, localOpen, lerp);
    }

    public override void TeleportClosed(){

        base.TeleportClosed();

        transform.localRotation = localClosed;
    }



}
