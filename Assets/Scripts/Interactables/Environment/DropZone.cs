using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class DropZoneEvents
{
    public UnityEvent OnCorrectItem;
    public UnityEvent OnWrongItem;
    public UnityEvent OnRemoveItem;
}

// This is a base class for useable (paint brush)
// and missing pieces
public class DropZone : Interactable
{

    public DropZoneEvents dropZoneEvents;

    public InventoryItem acceptedItem;

    Collider zone;

    InventoryItem zoneItem;

    public GameObject visual;

    private PuzzleElement puzzleElement;

     bool shouldShowVisual = true;

     public Transform placementLocation;

     public bool useDropColliderForPickup;

    protected override void  Awake(){
        zone = GetComponent<Collider>();

        if(zone == null) Debug.LogError("No collider on drop zone", this);
        if(!zone.isTrigger) Debug.LogError("Drop zone collider not trigger", this);

        puzzleElement = GetComponent<PuzzleElement>();

        CheckVisual();

        if(placementLocation == null){
            placementLocation = new GameObject().transform;
            placementLocation.parent = transform;
            placementLocation.localPosition = Vector3.zero;
            placementLocation.localRotation = Quaternion.identity;
            placementLocation.localScale = Vector3.one;

        }



        base.Awake();
    }

    void Start(){
    //         zoneItem = GetComponentInChildren<InventoryItem>();
    // if(zoneItem != null) PlaceItem(zoneItem);
    }

    public override void Click(){

        if (zoneItem != null)
        {
            Debug.LogError("Drop zone should not be interactable while occupied", this);
            return;
        }

        InventoryManager.instance.PlaceInDropZone(this);


        base.Click();
    }

    public override void Interact(){


        if(useDropColliderForPickup){
            if(zoneItem != null) zoneItem.Pickup();
            else Debug.Log("item is null");
        }

        base.Interact();
    }

    public void ItemTaken(){
        zoneItem = null;
        Show();
        dropZoneEvents.OnRemoveItem.Invoke();
    }

    // called by the inventory manager
    public void PlaceItem(InventoryItem item){

        item.transform.parent = transform;
        item.transform.localPosition = placementLocation.localPosition;
        item.transform.localRotation = placementLocation.localRotation;
        item.transform.localScale = Vector3.Scale(item.initialScale, placementLocation.localScale) ;

        zoneItem = item;


        if(acceptedItem != null){

            if(item.puzzleTag.Equals(acceptedItem.puzzleTag)){
                dropZoneEvents.OnCorrectItem.Invoke();
                if(puzzleElement != null) puzzleElement.Solve(true);
            }else{
                dropZoneEvents.OnWrongItem.Invoke();
                if(puzzleElement != null) puzzleElement.Solve(false);
            }
        }

        item.SetZone(this);
    }


   

    public void Hide(){
        shouldShowVisual = false;
        CheckVisual();

        if(!useDropColliderForPickup){
            zone.enabled = false;
        }
    }

    public void Show(){
        shouldShowVisual = true;
        CheckVisual();
        if(!useDropColliderForPickup){
            zone.enabled = true;
        }
    }

    void CheckVisual(){
        if(shouldShowVisual && isHighlighted){
            // visual.SetActive(true);
            InventoryManager.instance.GhostHand(this);
        }else{
            InventoryManager.instance.GhostHand(null);
            // visual.SetActive(false);
        }

        if(useDropColliderForPickup && zoneItem != null){
            zoneItem.Highlight(isHighlighted);
        }
    }

    public override void Highlight(bool highlight){

        if(highlight == isHighlighted) return;

        isHighlighted = highlight;
        CheckVisual();

    }




}