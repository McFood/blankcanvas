using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;



// This is a base class for useable (paint brush)
// and missing pieces
public class BasicIndicator : MonoBehaviour
{

    public Material on;
    public Material off;
    public Material intermediate;

    MeshRenderer meshRenderer;

    void Awake(){
        meshRenderer = GetComponent<MeshRenderer>();
    }
    
    public void SetOn(){
        meshRenderer.material = on;
    }

    public void SetOff(){
        meshRenderer.material = off;
    }

    public void SetIntermediate(){
        meshRenderer.material = intermediate;
    }



}
