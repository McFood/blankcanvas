using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;


public class DoorSliding : BaseDoor
{



    protected Vector3 localClosed;
    protected Vector3 localOpen;





    protected override void Awake(){

        base.Awake();

        if(startOpen){
            localClosed = transform.localPosition + transform.localRotation * direction * distance;
            localOpen = transform.localPosition;
        }else{
            localClosed = transform.localPosition;
            localOpen = transform.localPosition + transform.localRotation * direction * distance;
        }
    }




    protected override void Movement(float lerp){

        transform.localPosition = Vector3.Lerp(localClosed, localOpen, lerp);
    }

    public override void TeleportClosed(){

        base.TeleportClosed();

        transform.localPosition = localClosed;

        // Debug.Log("Teleport Closed: " + localClosed, this);
    }



}
