using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

using UnityEngine.Events;

[Serializable]
public class DoorEvents{
    public UnityEvent onOpen;
    public UnityEvent onClose;
}


public abstract class BaseDoor : Interactable
{

    public DoorEvents doorEvents;

    

    [Header("Movement")]
    public Vector3 direction = Vector3.forward;
    public float speed = 10;
    public float closeSpeed;

    public float distance = 1;

    

    

    protected bool closing;
    protected bool opening;

    protected bool isOpen;
    protected bool isClosed;

    public AnimationCurve openCurve = AnimationCurve.Linear(0,0,1,1);
    public AnimationCurve closeCurve = AnimationCurve.Linear(0,0,1,1);

    [Header("Options")]
    public bool isInteractable = true;
    public bool startOpen;


    Rigidbody rb;


    public override void Interact(){
        if(!isInteractable) return;

        Toggle();

        base.Interact();
    }


    protected override void Awake(){
        rb = GetComponent<Rigidbody>();

        if(rb) rb.isKinematic = true;

        direction = direction.normalized;

        if(startOpen){
            isOpen = true;
            isClosed = false;
        }else{
            isOpen = false;
            isClosed = true;
        }

        if(closeSpeed == 0) closeSpeed = speed;

        base.Awake();

    }

    public void Open(){
        opening = true;
        closing = false;

        isOpen = false;
        isClosed = false;
    }

    public void Close(){
        closing = true;
        opening = false;

        isOpen = false;
        isClosed = false;
    }

    public void Toggle(){
        if(isOpen || opening){
            Close();
        }else if(isClosed || closing){
            Open();
        }else{
            Debug.LogError("Toggle: Door not open or closed", this);
        }
    }

    float lerp;

    void Lerp(){
        if(opening){
            lerp += Time.deltaTime / (distance / speed);
            
            if(lerp >= 1){
                lerp = 1;
                isOpen = true;
                opening = false;
                // Debug.Log("onOpen", this);
                doorEvents.onOpen.Invoke();
            }

            Movement(openCurve.Evaluate(lerp));
        }

        if(closing){
            lerp -= Time.deltaTime / (distance / closeSpeed);

            if(lerp <= 0){
                lerp = 0;
                isClosed = true;
                closing = false;
                // Debug.Log("onClose", this);
                doorEvents.onClose.Invoke();
            }

            Movement( 1 - closeCurve.Evaluate( 1 - lerp));
        }

        
    }

    public void Update(){
        Lerp();
    }

    protected abstract void Movement(float lerp);

    public virtual void TeleportClosed(){

        lerp = 0;
        
        isClosed = true;
        isOpen = false;

        closing = false;
        opening = false;

    }


}
