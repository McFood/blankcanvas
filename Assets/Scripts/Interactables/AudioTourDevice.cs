using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

using UnityEngine.Events;


// This is a base class for useable (paint brush)
// and missing pieces
[RequireComponent(typeof(AudioSource))]
public class AudioTourDevice : InventoryItem
{

    AudioSource source;

    protected override void Awake(){
        source = GetComponent<AudioSource>();

        isDropable = false;

        base.Awake();
    }

    protected override void CheckPuzzleTag(){}

    void Update(){

        // check if in hand

        CheckInputs();


        // check for number input

        // do error sound if wrong
    }

    void CheckInputs(){

        if(!inHand) return;

        if( Input.GetKeyUp(KeyCode.Alpha0) || Input.GetKeyUp(KeyCode.Keypad0)){
            DoInput(0);
        }
        if( Input.GetKeyUp(KeyCode.Alpha1) || Input.GetKeyUp(KeyCode.Keypad1)){
            DoInput(1);
        }
        if( Input.GetKeyUp(KeyCode.Alpha2) || Input.GetKeyUp(KeyCode.Keypad2)){
            DoInput(2);
        }
        if( Input.GetKeyUp(KeyCode.Alpha3) || Input.GetKeyUp(KeyCode.Keypad3)){
            DoInput(3);
        }
        if( Input.GetKeyUp(KeyCode.Alpha4) || Input.GetKeyUp(KeyCode.Keypad4)){
            DoInput(4);
        }
        if( Input.GetKeyUp(KeyCode.Alpha5) || Input.GetKeyUp(KeyCode.Keypad5)){
            DoInput(5);
        }
        if( Input.GetKeyUp(KeyCode.Alpha6) || Input.GetKeyUp(KeyCode.Keypad6)){
            DoInput(6);
        }
        if( Input.GetKeyUp(KeyCode.Alpha7) || Input.GetKeyUp(KeyCode.Keypad7)){
            DoInput(7);
        }
        if( Input.GetKeyUp(KeyCode.Alpha8) || Input.GetKeyUp(KeyCode.Keypad8)){
            DoInput(8);
        }
        if( Input.GetKeyUp(KeyCode.Alpha9) || Input.GetKeyUp(KeyCode.Keypad9)){
            DoInput(9);
        }

    }

    int inputIndex;
    int currentInput;

    void DoInput(int entry){
        int multipler =  (int)Math.Pow(10, (double)(2 - inputIndex));
        currentInput += entry * multipler;
        inputIndex++;

        PlayInputSound();

        if(inputIndex >= 3){
            inputIndex = 0;
            CompleteInput();
        }
    }

    void CompleteInput(){
        Debug.Log("Checking for audio entry: " + currentInput);
        bool success = false;
        foreach(AudioTourEntry entry in AudioTourManager.instance.entries){
            if(currentInput == entry.number){
                StartCoroutine(PlayEntryDelay(entry));
                success = true;
                break;
            }
        }

        currentInput = 0;
        inputIndex = 0;

        if(!success) PlayErrorSound();


    }

    IEnumerator PlayEntryDelay(AudioTourEntry entry){
        yield return new WaitForSeconds(0.5f);
        PlayEntrySound(entry);
    }

    void PlayEntrySound(AudioTourEntry entry){
        source.clip = entry.clip;
        if(source.clip != null)source.Play();
        else Debug.LogError("No clip for audio tour entry " + entry.number);
    }

    void PlayErrorSound(){
        source.clip = AudioTourManager.instance.errorSound;
        if(source.clip != null) source.Play();
    }

    void PlayInputSound(){
        source.clip = AudioTourManager.instance.inputSound;
        if(source.clip != null) source.Play();
    }

}