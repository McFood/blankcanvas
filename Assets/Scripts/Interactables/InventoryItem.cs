using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

using UnityEngine.Events;


// This is a base class for useable (paint brush)
// and missing pieces
public class InventoryItem : Interactable
{

    public String puzzleTag;

    protected bool inInventory;
    protected bool inHand;

    public DropZone occupiedZone {get; private set;}

    // so we don't accidentally drop the paintbrush
    public bool isDropable = true;
    public bool skipInventoryAnimations = false;

    new Collider collider;

    public Transform itemHoldPoint;

    public Vector3 initialScale{get; private set;} 

    protected override void Awake(){
        CheckPuzzleTag();

        initialScale = transform.localScale;

        collider = GetComponent<Collider>();

        SetUpGhost();

        if(itemHoldPoint == null){
            itemHoldPoint = new GameObject().transform;
            itemHoldPoint.transform.parent = transform;
            itemHoldPoint.transform.localPosition = Vector3.zero;
            itemHoldPoint.transform.localRotation = Quaternion.identity;
            itemHoldPoint.transform.localScale = Vector3.one;

        }

        base.Awake();
    }

    public void AttachToTransform(Transform newParent){

        transform.parent = newParent;
        transform.localRotation = Quaternion.Inverse(itemHoldPoint.localRotation);
        transform.localPosition = Vector3.Scale(itemHoldPoint.localPosition, transform.lossyScale);

    }


    void SetUpGhost(){
        MeshFilter filter = GetComponent<MeshFilter>();

        if(filter == null) return;

        ghost = new GameObject();
        ghost.transform.parent = transform;
        ghost.transform.localPosition = Vector3.zero;
        ghost.transform.localRotation = Quaternion.identity;
        ghost.transform.localScale = Vector3.one;

        MeshFilter ghostFilter = ghost.AddComponent<MeshFilter>();
        ghostFilter.mesh = filter.mesh;

        MeshRenderer GhostRender = ghost.AddComponent<MeshRenderer>();
        GhostRender.material = GameManager.instance.ghostMaterial;

        ghost.SetActive(false);
    }

    GameObject ghost;
    DropZone ghostZone;

    public virtual void Ghost(DropZone zone){

        if(zone == ghostZone) return;

        ghostZone = zone;

        if(ghost != null){
            if(ghostZone != null){
                ghost.SetActive(true );
                ghost.transform.parent = ghostZone.transform;
                ghost.transform.position = ghostZone.placementLocation.position;
                ghost.transform.rotation = ghostZone.placementLocation.rotation;
                ghost.transform.localScale = Vector3.Scale(initialScale, ghostZone.placementLocation.localScale);
                Debug.Log("init scale: " + initialScale + " localscale " + ghostZone.placementLocation.localScale);
            }else{
                ghost.SetActive(false);
            }
        }

    }


    protected virtual void CheckPuzzleTag(){
        if(puzzleTag.Equals("")){
            Debug.LogWarning("No puzzle tag on inventory item", this);
        }
    }

    public void SetCollider(bool enabled){
        if(collider == null) return;

        collider.enabled = enabled;
    }

    public void SetZone(DropZone dropZone){
        occupiedZone = dropZone;
        occupiedZone.Hide();

        // This might be better somewhere else
        Drop();
    }

    public void ClearZone(){

        if(occupiedZone == null) return;

        occupiedZone.ItemTaken();
        occupiedZone = null;


    }
    
    public void Pickup(){
        ClearZone();
        Highlight(false);
        InventoryManager.instance.AddItem(this);
        inInventory = true;
        inHand = true;
    }

    public void Drop(){
        Ghost(null);
        inInventory = false;
        
    }

    public void Holster(){
        inHand = false;
        Ghost(null);
    }

    public void Unholster(){
        inHand = true;
    }


    public override void Interact(){

        if(inInventory) {
            Debug.LogError("Trying to interact with item in inventroy");
            return;
        }

        Pickup();

        base.Interact();

    }


}