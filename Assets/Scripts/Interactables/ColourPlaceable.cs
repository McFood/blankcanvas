﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourPlaceable : Interactable
{
    public new Renderer renderer;
    public int materialIndex = 0;
    public PuzzleElement puzzleElement;
    public Material expectedMaterial;

    protected override void Awake()
    {
        base.Awake();
        if (renderer == null)
        {
            renderer = GetComponent<Renderer>();
        }
        if (puzzleElement == null)
        {
            puzzleElement = GetComponent<PuzzleElement>();
        }
    }

    public override void Click()
    {
        base.Click();
        InventoryItem currentItem = InventoryManager.instance.CurrentItem;
        if (currentItem == null)
            return;

        PaintBrush paintBrush = currentItem.GetComponent<PaintBrush>();
        if (paintBrush == null)
            return;

        SetColor(paintBrush.Color);
    }

    public void SetColor(Material color)
    {
        puzzleElement.Solve(color == expectedMaterial);

        Material[] materials = renderer.materials;
        materials[materialIndex] = color;
        renderer.materials = materials;
    }

}
