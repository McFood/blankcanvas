﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintBrush : MonoBehaviour
{
    private Material color;
    public Material Color {
        get => color;
        set {
            color = value;
            brushRenderer.material = color;
        }
    }

    public Renderer brushRenderer;
    void Awake()
    {
        if (brushRenderer == null)
        {
            brushRenderer = GetComponent<Renderer>();
        }
    }
}
