﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

[Serializable]
public class InteractableEvents{
    public UnityEvent onInteract;

}

public class Interactable : MonoBehaviour
{

    public InteractableEvents interactableEvents;
    public InteractableEvents clickEvents;

    protected virtual void Awake(){

        SetupHighlight();
        
    }

    protected virtual void SetupHighlight(){

        MeshFilter filter = GetComponent<MeshFilter>();
        // if(filter == null) filter = GetComponentInChildren<MeshFilter>();


        if(filter == null) return;


        highlighter = new GameObject();
        highlighter.transform.parent = transform;
        highlighter.transform.localPosition = Vector3.zero;
        highlighter.transform.localRotation = Quaternion.identity;
        highlighter.transform.localScale = Vector3.one;

        MeshFilter highlightFilter = highlighter.AddComponent<MeshFilter>();
        highlightFilter.mesh = filter.mesh;

        MeshRenderer highlightRender = highlighter.AddComponent<MeshRenderer>();
        highlightRender.material = GameManager.instance.highlightMaterial;

        highlighter.SetActive(false);
    }

    GameObject highlighter;
    protected bool isHighlighted;
    public virtual void Highlight(bool highlight){

        if(highlight == isHighlighted) return;

        isHighlighted = highlight;

        if(highlighter != null){
            highlighter.SetActive(isHighlighted);
        }

    }

    public virtual void Interact()
    {
        Debug.Log("Interacted with: " + name, this);
        // Override Me.
        interactableEvents.onInteract.Invoke();
    }

    public virtual void Click()
    {
        Debug.Log("Clicked on: " + name, this);
        // Override Me.
        clickEvents.onInteract.Invoke();
    }
}
