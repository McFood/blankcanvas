﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaqueAudioController : Interactable
{

    public AudioClip plaqueClip;

    Animator buttonAnimator;
    AudioSource buttonAudioSource;

    void Start()
    {
        buttonAnimator = gameObject.GetComponent<Animator>();
        buttonAudioSource = gameObject.GetComponent<AudioSource>();
        buttonAudioSource.clip = plaqueClip;
    }

    public override void Interact()
    {
        base.Interact();
        buttonAnimator.SetTrigger("pressButton");
        if (buttonAudioSource.isPlaying)
        {
            buttonAudioSource.Stop();
        }
        else
        {
            buttonAudioSource.Play();
        }
        
    }




   
}
